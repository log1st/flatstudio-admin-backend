<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:15
 */

$params = array_merge(
	require __DIR__ . '/../../common/config/params.php',
	require __DIR__ . '/../../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id' => 'console',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'console\controllers',
	'components' => [
		'urlManager' => [
			'rules' => [
				'api/<entity:[\w]+>' => 'api/<entity>'
			]
		]
	],
	'params' => $params
];