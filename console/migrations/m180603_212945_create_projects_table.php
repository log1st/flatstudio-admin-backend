<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m180603_212945_create_projects_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('projects', [
			'id' => $this->primaryKey(),
			'isFeatured' => $this->integer(1),
			'name' => $this->string(),
			'slogan' => $this->string(),
			'status' => $this->integer(1),
			'publishedAt' => $this->integer(),
			'coverDesktop' => $this->string(),
			'coverMobile' => $this->string(),
			'thumbnailDesktop' => $this->string(),
			'thumbnailMobile' => $this->string(),
			'client' => $this->string(),
			'task' => $this->string(),
			'roles' => $this->string(),
			'link' => $this->string(),
			'tags' => $this->string(),
			'brandColor' => $this->string(),
			'layout' => $this->string(),
			'overviewLogo' => $this->string(),

			'overviewText' => $this->string(),
			'overviewBackgroundDesktop' => $this->string(),
			'overviewBackgroundMobile' => $this->string(),

			'overviewTileImage1' => $this->string(),
			'overviewTileLabel1' => $this->string(),
			'overviewTileDescription1' => $this->string(),
			'overviewTileImage2' => $this->string(),
			'overviewTileLabel2' => $this->string(),
			'overviewTileDescription2' => $this->string(),
			'overviewTileImage3' => $this->string(),
			'overviewTileLabel3' => $this->string(),
			'overviewTileDescription3' => $this->string(),

			'overviewBackgroundColor' => $this->string(),

			'customTheme' => $this->integer(1),

			'backgroundColor' => $this->string(),
			'primaryFontColor' => $this->string(),
			'secondaryFontColor' => $this->string(),
			'dotColor' => $this->string(),
			'backgroundImageDesktop' => $this->string(),
			'backgroundImageMobile' => $this->string(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('projects');
	}
}
