<?php
	
	use yii\db\Migration;
	
	/**
	 * Class m180614_193556_addActiveAndOrderInProjects
	 */
	class m180614_193556_addOrderInProjects extends Migration
	{
		/**
		 * {@inheritdoc}
		 */
		public function safeUp()
		{
			$this->addColumn('projects', 'order', $this->integer());
		}
		
		/**
		 * {@inheritdoc}
		 */
		public function safeDown()
		{
			$this->dropColumn('projects', 'order');
			
			return true;
		}
	}
