<?php

use yii\db\Migration;

/**
 * Handles the creation of table `components`.
 */
class m180615_032721_create_components_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('components', [
			'id' => $this->primaryKey(),
			'project' => $this->integer(),
			'type' => $this->string(),
			'meta' => $this->text(),
			'order' => $this->integer()
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('components');

		return true;
	}
}
