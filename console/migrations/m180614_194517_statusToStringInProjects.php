<?php
	
	use yii\db\Migration;
	
	/**
	 * Class m180614_194517_statusToStringInProjects
	 */
	class m180614_194517_statusToStringInProjects extends Migration
	{
		/**
		 * {@inheritdoc}
		 */
		public function safeUp()
		{
			$this->alterColumn('projects', 'status', $this->string());
		}
		
		/**
		 * {@inheritdoc}
		 */
		public function safeDown()
		{
			$this->alterColumn('projects', 'status', $this->integer()->defaultValue(1));
			
			return true;
		}
	}
