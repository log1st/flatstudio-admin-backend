<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 30.04.2018
 * Time: 4:55
 */

$params = array_merge(
	require __DIR__ . '/../common/config/params.php',
	require __DIR__ . '/../common/config/params-local.php',
	require __DIR__ . '/params.php',
	require __DIR__ . '/params-local.php'
);

return [
	'id' => 'app',
	'basePath' => dirname(__DIR__),
	'controllerNamespace' => 'app\controllers',
	'components' => [
		'urlManager' => [
			'rules' => [
				'schema' => 'schema/index',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<id:\d+>/<action:\w+>' => '<controller>/<action>',
			]
		]
	],
	'params' => $params
];