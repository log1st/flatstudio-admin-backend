<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:46
 */

namespace app\components;


use yii\helpers\ArrayHelper;

class BaseJsonController extends BaseController
{
	public function jsonResponse($status = 200, $message = null, $response = null)
	{
		return $this->asJson(ArrayHelper::merge([
			'status' => $status
		], $message !== null ? [
			'message' => $message
		] : [], $response !== null ? [
			'response' => $response
		] : []));
	}
}