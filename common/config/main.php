<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:15
 */

return [
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
	],
	'language' => 'en-US',
	'vendorPath' => dirname(__DIR__, 2) . '/vendor',
	'container' => [
		'definitions' => [
			\app\src\contracts\ProjectSchemeContract::class => \app\src\concretes\ProjectScheme::class,
			\app\src\contracts\ProjectTransformerContract::class => \app\src\concretes\ProjectTransformer::class,

			\app\src\contracts\FieldsRepositoryContract::class => \app\src\concretes\FieldsRepository::class,
		]
	],
	'components' => [
		'urlManager' => [
			'showScriptName' => false,
			'enablePrettyUrl' => true
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
			'cachePath' => '@runtime/cache',
		],
		'request' => [
			'enableCookieValidation' => true,

			'csrfCookie' => [
				'name' => '_csrf',
				'path' => '/',
				'domain' => "." . YII_HOST,
			],
		],
		'assetManager' => [
			'forceCopy' => !YII_CACHE,
			'converter' => [
				'class' => 'yii\web\AssetConverter',
				'forceConvert' => !YII_CACHE,
			],
		],
		'user' => [
			'identityCookie' => [
				'name' => '_identity',
				'domain' => '.' . YII_HOST
			]
		],
		'session' => [
			'cookieParams' => [
				'lifetime' => 10 * 365 * 24 * 60 * 60,
				'domain' => "." . YII_HOST,
				'httpOnly' => true,
			],
		],
		'errorHandler' => [
			'errorAction' => 'error/index',
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => [ 'error', 'warning' ],
				],
			],
		],
	],
	'modules' => \yii\helpers\ArrayHelper::merge(YII_DEBUG ? [
		'debug' => [
			'class' => \yii\debug\Module::class,
			'allowedIPs' => ['*'],
			'traceLine' => '<a href="phpstorm://open?url={file}&line={line}">{file}:{line}</a>',
			'dataPath' => '@runtime/debugbar',
		],
	] : [], [

	]),
	'bootstrap' => \yii\helpers\ArrayHelper::merge(YII_DEBUG ? ['debug'] : [], [

	]),
];