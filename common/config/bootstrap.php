<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:19
 */

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@app', dirname(__DIR__, 2) . '/');
Yii::setAlias('@console', dirname(__DIR__, 2) . '/console');

date_default_timezone_set('UTC');
