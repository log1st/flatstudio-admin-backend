<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:19
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

defined('YII_ENV') or define('YII_END', 'dev');
defined('YII_DEBUG') or define('YII_DEBUG', true);

defined('YII_CACHE') or define('YII_CACHE', true);
defined('YII_CACHE_TIMEOUT') or define('YII_CACHE_TIMEOUT', 1 * 24 * 60 * 60);

defined('YII_HOST') or define('YII_HOST', 'localhost');
