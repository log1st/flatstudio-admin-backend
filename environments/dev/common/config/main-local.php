<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:16
 */

return [
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=flatata',
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		],
		'request' => [
			'cookieValidationKey' => ''
		]
	],
];