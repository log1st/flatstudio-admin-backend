#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 22.04.2018
 * Time: 10:15
 */


require __DIR__ . '/common/config/bootstrap-local.php';
require __DIR__ . '/console/config/bootstrap-local.php';
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/common/config/bootstrap.php';
require __DIR__ . '/console/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
	require __DIR__ . '/common/config/main.php',
	require __DIR__ . '/common/config/main-local.php',
	require __DIR__ . '/console/config/main.php',
	require __DIR__ . '/console/config/main-local.php'
);

foreach(['request', 'session', 'user', 'errorHandler'] as $item) {
	if(array_key_exists($item, $config['components'])) {
		unset($config['components'][$item]);
	}
}

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);