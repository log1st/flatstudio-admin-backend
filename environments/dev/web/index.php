<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 22.04.2018
 * Time: 10:22
 */

require __DIR__ . '/../common/config/bootstrap-local.php';
require __DIR__ . '/../config/bootstrap-local.php';
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
	require __DIR__ . '/../common/config/main.php',
	require __DIR__ . '/../common/config/main-local.php',
	require __DIR__ . '/../config/main.php',
	require __DIR__ . '/../config/main-local.php'
);

(new yii\web\Application($config))->run();