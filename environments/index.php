<?php
/**
 * Created by PhpStorm.
 * User: log1st
 * Date: 30.04.2018
 * Time: 4:57
 */

$base = [
	'setWritable' => [
		'/runtime',
		'/web'
	],
	'setExecutable' => [
		'yii.php',
	],
	'setCookieValidationKey' => [
		'/common/config/main-local.php',
	],
];

return [
	'dev' => array_merge_recursive($base, [
		'path' => 'dev',
	]),
	'dev web' => array_merge_recursive($base, [
		'path' => 'dev-web',
	])
];