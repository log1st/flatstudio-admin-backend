<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\helpers\ArrayHelper;

class TabsComponent extends Component
{
	public $title1;
	public $title2;
	public $title3;
	public $image1;
	public $imageMobile1;
	public $image2;
	public $imageMobile2;
	public $image3;
	public $imageMobile3;
	
	public function getMetaKeys(): array
	{
		return ['image1', 'imageMobile1', 'image2', 'imageMobile2', 'image3', 'imageMobile3', 'title'];
	}
	
	public function scenarios()
	{
		$fields = [
			'title1',
			'title2',
			'title3',
			'image1',
			'imageMobile1',
			'image2',
			'imageMobile2',
			'image3',
			'imageMobile3',
		];
		
		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}
	
	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			['image1', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title1);
			}],
			['imageMobile1', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title1);
			}],
			['image2', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title2);
			}],
			['imageMobile2', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title2);
			}],
			['image3', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title3);
			}],
			['imageMobile3', 'required', 'when' => function(TabsComponent $model) {
				return !empty($model->title3);
			}],
		]);
	}
}