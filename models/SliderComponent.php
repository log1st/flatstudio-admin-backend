<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\helpers\ArrayHelper;

class SliderComponent extends Component
{
	public $title;
	public $image1;
	public $imageMobile1;
	public $image2;
	public $imageMobile2;
	public $image3;
	public $imageMobile3;
	
	public function getMetaKeys(): array
	{
		return ['image1', 'imageMobile1', 'image2', 'imageMobile2', 'image3', 'imageMobile3', 'title'];
	}
	
	public function scenarios()
	{
		$fields = [
			'title',
			'image1',
			'imageMobile1',
			'image2',
			'imageMobile2',
			'image3',
			'imageMobile3',
		];
		
		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}
	
	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
//			['title', 'required'],
			['image1', 'required'],
			['imageMobile1', 'required'],
			['image2', 'required'],
			['imageMobile2', 'required'],
			['image3', 'required'],
			['imageMobile3', 'required'],
		]);
	}
}