<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 04.06.2018
	 * Time: 5:52
	 */
	
	namespace app\models;
	
	
	use app\components\ActiveRecord;
	use yii\db\Query;
	use yii\helpers\ArrayHelper;
	
	/**
	 * Class Project
	 * @package app\models
	 *
	 * @property $id
	 * @property $isFeatured
	 * @property $name
	 * @property $slogan
	 * @property $status
	 * @property $publishedAt
	 * @property $coverDesktop
	 * @property $coverMobile
	 * @property $thumbnailDesktop
	 * @property $thumbnailMobile
	 * @property $client
	 * @property $task
	 * @property $roles
	 * @property $link
	 * @property $tags
	 * @property $brandColor
	 * @property $layout
	 * @property $overviewLogo
	 *
	 * @property $overviewText
	 * @property $overviewBackgroundDesktop
	 * @property $overviewBackgroundMobile
	 *
	 * @property $overviewTileImage1
	 * @property $overviewTileLabel1
	 * @property $overviewTileDescription1
	 * @property $overviewTileImage2
	 * @property $overviewTileLabel2
	 * @property $overviewTileDescription2
	 * @property $overviewTileImage3
	 * @property $overviewTileLabel3
	 * @property $overviewTileDescription3
	 *
	 * @property $overviewBackgroundColor
	 *
	 * @property $customTheme
	 *
	 * @property $backgroundColor
	 * @property $primaryFontColor
	 * @property $secondaryFontColor
	 * @property $dotColor
	 * @property $backgroundImageDesktop
	 * @property $backgroundImageMobile
	 *
	 * @property $order
	 *
	 * @property Component[] $components
	 */
	class Project extends ActiveRecord
	{
		
		const TYPE_PUBLISHED = 'published';
		const TYPE_SUPPORT = 'support';
		const TYPE_DRAFT = 'draft';
		const TYPE_ARCHIVED = 'archived';
		
		const IS_FEATURED_TRUE = 1;
		const IS_FEATURED_FALSE = 0;
		
		const LAYOUT_TRIPLE = 'triple';
		const LAYOUT_SINGLE = 'single';
		
		const CUSTOM_THEME_TRUE = 1;
		const CUSTOM_THEME_FALSE = 0;
		
		public static function tableName()
		{
			return 'projects';
		}
		
		public function attributeLabels()
		{
			return ArrayHelper::merge(parent::attributeLabels(), [
				'overviewTileImage1' => 'First overview tile image',
				'overviewTileLabel1' => 'First overview tile label',
				'overviewTileDescription1' => 'First overview tile description',
				'overviewTileImage2' => 'Second overview tile image',
				'overviewTileLabel2' => 'Second overview tile label',
				'overviewTileDescription2' => 'Second overview tile description',
				'overviewTileImage3' => 'Third overview tile image',
				'overviewTileLabel3' => 'Third overview tile label',
				'overviewTileDescription3' => 'Third overview tile description',
			]);
		}
		
		public function scenarios()
		{
			$attributes = [
				'isFeatured',
				'name',
				'slogan',
				'status',
				'publishedAt',
				
				'coverDesktop',
				'coverMobile',
				'thumbnailDesktop',
				'thumbnailMobile',
				
				'client',
				'task',
				'roles',
				'link',
				'tags',
				'brandColor',
				
				'layout',
				'overviewLogo',
				'overviewText',
				
				'overviewBackgroundDesktop',
				'overviewBackgroundMobile',
				
				'overviewTileImage1',
				'overviewTileLabel1',
				'overviewTileDescription1',
				'overviewTileImage2',
				'overviewTileLabel2',
				'overviewTileDescription2',
				'overviewTileImage3',
				'overviewTileLabel3',
				'overviewTileDescription3',
				
				'overviewBackgroundColor',
				
				'customTheme',
				
				'backgroundColor',
				'primaryFontColor',
				'secondaryFontColor',
				'dotColor',
				'backgroundImageDesktop',
				'backgroundImageMobile',
			];
			
			return [
				'insert' => $attributes,
				'update' => $attributes,
			];
		}
		
		protected function statusFields()
		{
			return [
				self::TYPE_PUBLISHED => 'Published',
				self::TYPE_SUPPORT => 'Support',
				self::TYPE_DRAFT => 'Draft',
				self::TYPE_ARCHIVED => 'Archived',
			];
		}
		
		protected function isFeaturedValues()
		{
			return [
				self::IS_FEATURED_TRUE => 'Yes',
				self::IS_FEATURED_FALSE => 'No',
			];
		}
		
		protected function layoutFields()
		{
			return [
				self::LAYOUT_TRIPLE => 'Triple',
				self::LAYOUT_SINGLE => 'Single',
			];
		}
		
		protected function customThemeFields()
		{
			return [
				self::CUSTOM_THEME_TRUE => 'Yes',
				self::CUSTOM_THEME_FALSE => 'No',
			];
		}
		
		public function afterFind()
		{
			parent::afterFind();
			
			$this->roles = json_decode($this->roles, true);
			$this->tags = json_decode($this->tags, true);
		}
		
		/**
		 * @param bool $insert
		 * @return bool
		 * @throws \yii\db\Exception
		 */
		public function beforeSave($insert)
		{
			if((int)$this->isFeatured === 1) {
				\Yii::$app->db->createCommand()->update(Project::tableName(), [
					'isFeatured' => 0
				])->execute();
			}
			
			$maxOrder = Project::find()->where(['status' => $this->status])->max('`order`');
			
			$this->order = (int)$maxOrder + 1;
			
			$this->roles = json_encode($this->roles);
			$this->tags = json_encode($this->tags);
			
			return parent::beforeSave($insert);
		}
		
		/**
		 * @return array
		 */
		public function rules()
		{
			return [
//				[['isFeatured'], 'required'],
				[['isFeatured'], 'filter', 'filter' => 'intval'],
				[['isFeatured'], 'in', 'range' => array_keys($this->isFeaturedValues())],
				
				[['name'], 'required'],
				
				[['slogan'], 'required'],
				
				[['status'], 'required'],
				[['status'], 'in', 'range' => array_keys($this->statusFields())],
				
				[['publishedAt'], 'required', 'when' => function (Project $project) {
					return $project->status === self::TYPE_PUBLISHED;
				}],
				
				[['coverDesktop', 'coverMobile', 'thumbnailDesktop', 'thumbnailMobile'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord;
				}],
//				[['coverDesktop', 'coverMobile', 'thumbnailDesktop', 'thumbnailMobile'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				]],
				
				[['client'], 'required'],
				[['task'], 'required'],
				
				[['roles'], 'required'],
				[['roles'], 'each', 'rule' => ['required']],
				
				[['link'], 'required'],
				[['link'], 'url', 'defaultScheme' => 'http'],
				
				[['tags'], 'required'],
				[['tags'], 'each', 'rule' => ['required']],
				
				[['brandColor'], 'required'],
				[['brandColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/'],
				
				[['layout'], 'required'],
				[['layout'], 'in', 'range' => array_keys($this->layoutFields())],
				
				[['overviewLogo'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord;
				}],
//				[['overviewLogo'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				]],
				[['overviewText'], 'required'],
				
				[['overviewBackgroundDesktop', 'overviewBackgroundMobile'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord && $project->layout === self::LAYOUT_SINGLE;
				}],
//				[['overviewBackgroundDesktop', 'overviewBackgroundMobile'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				], 'when' => function (Project $project) {
//					return $project->layout === self::LAYOUT_SINGLE;
//				}],
				
				[['overviewTileImage1', 'overviewTileImage2', 'overviewTileImage3'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord && $project->layout === self::LAYOUT_TRIPLE;
				}],
//				[['overviewTileImage1', 'overviewTileImage2', 'overviewTileImage3'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				], 'when' => function (Project $project) {
//					return $project->layout === self::LAYOUT_TRIPLE;
//				}],
				
				[['overviewTileLabel1', 'overviewTileLabel2', 'overviewTileLabel3', 'overviewTileDescription1', 'overviewTileDescription2', 'overviewTileDescription3'], 'required', 'when' => function (Project $project) {
					return $project->layout === self::LAYOUT_TRIPLE;
				}],
				
				[['overviewBackgroundColor'], 'required'],
				[['overviewBackgroundColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/'],
				
				[['customTheme'], 'filter', 'filter' => 'intval'],
				[['customTheme'], 'in', 'range' => array_keys($this->customThemeFields())],
				
				[['backgroundColor'], 'required', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				[['backgroundColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				
				[['primaryFontColor'], 'required', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				[['primaryFontColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				
				[['secondaryFontColor'], 'required', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				[['secondaryFontColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				
				[['dotColor'], 'required', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				[['dotColor'], 'match', 'pattern' => '/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/', 'when' => function (Project $project) {
					return $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
				
				[['backgroundImageDesktop'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord && $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
//				[['backgroundImageDesktop'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				]],
				
				[['backgroundImageMobile'], 'required', 'when' => function (Project $project) {
					return $project->isNewRecord && $project->customTheme === self::CUSTOM_THEME_TRUE;
				}],
//				[['backgroundImageMobile'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => [
//					'image/png', 'image/jpeg', 'image/jpg'
//				]],
			];
		}
		
		public function getComponents() {
			return $this->hasMany(Component::class, [
				'project' => 'id'
			]);
		}
	}