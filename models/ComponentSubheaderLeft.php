<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\base\Model;
use yii\helpers\ArrayHelper;

class ComponentSubheaderLeft extends Component
{
	public $icon;
	public $title;
	public $subTitle;
	
	public function getMetaKeys(): array
	{
		return ['icon', 'title', 'subTitle'];
	}

	public function scenarios()
	{
		$fields = [
			'icon',
			'title',
			'subTitle'
		];

		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
//			['image', 'required'],
			['title', 'required'],
			['subTitle', 'required'],
		]);
	}
}