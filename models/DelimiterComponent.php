<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\helpers\ArrayHelper;

class DelimiterComponent extends Component
{
	public $title;
	public $subTitle;
	public $date;
	
	public function getMetaKeys(): array
	{
		return ['date', 'title', 'subTitle'];
	}

	public function scenarios()
	{
		$fields = [
			'title',
			'date',
			'subTitle'
		];

		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			['title', 'required'],
			['date', 'required'],
			['subTitle', 'required'],
		]);
	}
}