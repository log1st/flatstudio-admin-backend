<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\base\Model;
use yii\helpers\ArrayHelper;

class ComponentSubheader extends Component
{
	public $title;
	public $subTitle;
	
	public function getMetaKeys(): array
	{
		return ['title', 'subTitle'];
	}

	public function scenarios()
	{
		$fields = [
			'title',
			'subTitle'
		];

		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
			['title', 'required'],
//			['subTitle', 'required'],
		]);
	}
}