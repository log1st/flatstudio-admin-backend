<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:33
 */

namespace app\models;


use app\components\ActiveRecord;

/**
 * Class Component
 * @package app\models
 *
 * @property $type
 * @property $project
 * @property $meta
 * @property $order
 */
class Component extends ActiveRecord
{
	public function __construct(array $config = [])
	{
		$this->meta = [];
		
		parent::__construct($config);
	}
	
	public static function tableName()
	{
		return 'components';
	}

	protected function getMetaKeys() : array {
		return [];
	}

	public function afterFind()
	{
		parent::afterFind();

		$this->meta = json_decode($this->meta, true);
		foreach($this->getMetaKeys() as $key => $value) {
			$this->{$key} = $value;
		}
	}

	public function scenarios()
	{
		$fields = ['project'];

		return [
			'validate' => $fields,
		];
	}

	public function rules() {
		return [
//			[['project'], 'required'],
//			[['project'], 'exist', 'targetClass' => Project::class, 'targetAttribute' => 'id']
		];
	}

	/**
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		$maxOrder = Component::find()->where(['project' => $this->project])->max('`order`');

		$this->order = (int)$maxOrder + 1;

		if(is_array($this->meta)) {
			$this->meta = json_encode($this->meta);
		}

		return parent::beforeSave($insert);
	}
}