<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 15.06.2018
 * Time: 13:28
 */

namespace app\models;


use yii\helpers\ArrayHelper;

class ImageComponent extends Component
{
	public $title;
	public $image;
	public $imageMobile;
	
	public function getMetaKeys(): array
	{
		return ['image', 'imageMobile', 'title'];
	}

	public function scenarios()
	{
		$fields = [
			'title',
			'image',
			'imageMobile'
		];

		return [
			'validate' => ArrayHelper::merge(parent::scenarios()['validate'], $fields),
		];
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [
//			['title', 'required'],
			['image', 'required'],
			['imageMobile', 'required'],
		]);
	}
}