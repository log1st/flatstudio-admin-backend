<?php
	/**
	 * Created by PhpStorm.
	 * User: log1s
	 * Date: 12.06.2018
	 * Time: 2:49
	 */
	
	namespace app\models;
	
	
	use app\components\UploadedFile;
	use yii\base\Model;
	
	class File extends Model
	{
		/** @var UploadedFile|string */
		public $file;
		
		public function scenarios()
		{
			return [
				'insert' => ['file']
			];
		}
		
		public function rules()
		{
			return [
				[['file'], 'required'],
				[['file'], 'file', 'skipOnEmpty' => true,
//					'mimeTypes' => [
//						'image/png', 'image/jpeg', 'image/webp', 'image/gif', 'video/mp4', 'image/svg+xml'
//					]
				],
			];
		}
	}