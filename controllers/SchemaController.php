<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 17.06.2018
	 * Time: 6:22
	 */
	
	namespace app\controllers;
	
	
	use app\components\BaseJsonController;
	use app\models\Component;
	use app\models\Project;
	
	class SchemaController extends BaseJsonController
	{
		public function actionIndex() {
			$projects = Project::find()->with('components')->all();
			
			return $this->jsonResponse(200, null, array_map(function(Project $project) {
				return [
					'attributes' => $project->attributes,
					'components' => array_map(function(Component $component) {
						return array_intersect_key($component->attributes, array_flip(['type', 'meta']));
					}, $project->components)
				];
			}, $projects));
		}
	}