<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:37
 */

namespace app\controllers\actions\projects;


use app\controllers\ProjectsController;
use app\models\Project;
use yii\base\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\projects
 *
 * @property ProjectsController $controller
 */
class IndexAction extends Action
{
	/**
	 * @return \yii\web\Response
	 */
	public function run()
	{
		return $this->controller->jsonResponse(200, null, array_map(function ($item) {
			/** @var Project $item */
			return $item->toArray();
		}, Project::find()->all()));
	}
}