<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 8:45
 */

namespace app\controllers\actions\projects;


use app\controllers\ProjectsController;
use app\models\Project;
use yii\base\Action;

/**
 * Class InsertAction
 * @package app\controllers\actions\projects
 *
 * @property ProjectsController $controller
 */
class InsertAction extends Action
{
	public function run()
	{
		$r = \Yii::$app->request;

		$model = new Project([
			'scenario' => 'insert'
		]);

		$model->load($r->post(), '');

		if (!$model->validate()) {
			return $this->controller->jsonResponse(400, 'Error while validation', [
				'errors' => $model->getErrors()
			]);
		}

		if (!$model->save(false)) {
			return $this->controller->jsonResponse(500, 'Unable to save');
		}

		return $this->controller->jsonResponse(200, 'Project has been added', [
			'id' => $model->id
		]);
	}
}