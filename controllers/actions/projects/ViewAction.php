<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 8:57
 */

namespace app\controllers\actions\projects;


use app\controllers\ProjectsController;
use app\models\Component;
use app\models\Project;
use yii\base\Action;

/**
 * Class ViewAction
 * @package app\controllers\actions\projects
 *
 * @property ProjectsController $controller
 */
class ViewAction extends Action
{
	/**
	 * @param $id
	 * @return \yii\web\Response
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\di\NotInstantiableException
	 */
	public function run($id) {
		$model = Project::find()->where([
			'id' => $id
		])->one();

		if(!$model) {
			if (!$model) {
				return $this->controller->jsonResponse(404, 'Project not found');
			}
		}

		return $this->controller->jsonResponse(200, null, [
			'project' => $model->toArray(),
			'components' => array_map(function(Component $item) {
				return [
					'type' => $item->type,
					'meta' => $item->meta
				];
			}, Component::findAll(['project' => $id]))
		]);
	}
}