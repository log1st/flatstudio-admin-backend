<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:37
 */

namespace app\controllers\actions\projects;


use app\controllers\ProjectsController;
use app\models\Project;
use yii\base\Action;

/**
 * Class UpdateAction
 * @package app\controllers\actions\projects
 *
 * @property ProjectsController $controller
 */
class UpdateAction extends Action
{
	public function run($id)
	{
		$r = \Yii::$app->request;

		$model = Project::find()->where([
			'id' => $id
		])->one();
		
		$model->setScenario('update');

		if (!$model) {
			return $this->controller->jsonResponse(404, 'Project not found');
		}
		
		$model->load($r->post(), '');

		if (!$model->validate()) {
			return $this->controller->jsonResponse(400, 'Error while validation', [
				'errors' => $model->getErrors()
			]);
		}

		if (!$model->save(false)) {
			return $this->controller->jsonResponse(500, 'Unable to save');
		}

		return $this->controller->jsonResponse(200, 'Project has been updated', [
			'id' => $model->id
		]);
	}
}