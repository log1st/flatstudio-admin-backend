<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 17.06.2018
	 * Time: 6:27
	 */
	
	namespace app\controllers\actions\components;
	
	
	use app\controllers\ComponentsController;
	use app\models\Component;
	use yii\base\Action;
	
	/**
	 * Class IndexAction
	 * @package app\controllers\actions\components
	 *
	 * @property ComponentsController $controller
	 */
	class IndexAction extends Action
	{
		public function run($project) {
			
			return $this->controller->jsonResponse(200, null, [
				'components' => array_map(function(Component $component) {
					return [
						'type' => $component->type,
						'meta' => $component->meta
					];
				}, Component::find()->where(['project' => $project])->all())
			]);
			
		}
	}