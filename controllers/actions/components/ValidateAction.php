<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 8:45
 */

namespace app\controllers\actions\components;


use app\controllers\ProjectsController;
use app\models\Component;
use app\models\ComponentHeader;
use app\models\ComponentSubheader;
use app\models\ComponentSubheaderLeft;
use app\models\ComponentSubheaderRight;
use app\models\DelimiterComponent;
use app\models\ImageComponent;
use app\models\Images2Component;
use app\models\Images3Component;
use app\models\Project;
use app\models\SliderComponent;
use app\models\SplitterComponent;
use app\models\TabsComponent;
use yii\base\Action;

/**
 * Class InsertAction
 * @package app\controllers\actions\projects
 *
 * @property ProjectsController $controller
 */
class ValidateAction extends Action
{
	protected $components = [
		'header' => ComponentHeader::class,
		'subheader' => ComponentSubheader::class,
		'subheaderRight' => ComponentSubheaderRight::class,
		'subheaderLeft' => ComponentSubheaderLeft::class,
		'delimiter' => DelimiterComponent::class,
		'image' => ImageComponent::class,
		'images2' => Images2Component::class,
		'images3' => Images3Component::class,
		'splitter' => SplitterComponent::class,
		'slider' => SliderComponent::class,
		'tabs' => TabsComponent::class,
	];

	public function run($type)
	{
		$r = \Yii::$app->request;

		$class = $this->components[$type];

		/** @var Component $model */
		$model = new $class([
			'scenario' => 'validate'
		]);
		
		$model->load($r->post(), '');
		
		if (!$model->validate()) {
			return $this->controller->jsonResponse(400, 'Error while validation', [
				'errors' => $model->getErrors()
			]);
		}

		return $this->controller->jsonResponse(200, 'Component has been validated');
	}
}