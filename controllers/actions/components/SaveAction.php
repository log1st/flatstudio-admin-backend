<?php
	/**
	 * Created by PhpStorm.
	 * User: Sergey
	 * Date: 17.06.2018
	 * Time: 6:27
	 */
	
	namespace app\controllers\actions\components;
	
	
	use app\controllers\ComponentsController;
	use app\models\Component;
	use yii\base\Action;
	
	/**
	 * Class SaveAction
	 * @package app\controllers\actions\components
	 *
	 * @property ComponentsController $controller
	 */
	class SaveAction extends Action
	{
		public function run($project)
		{
			$components = \Yii::$app->request->post('components') ?? [];
			
			Component::deleteAll([
				'project' => $project
			]);
			
			foreach ($components as $component) {
				try {
					(new Component([
						'scenario' => 'validate',
						'type' => $component['type'],
						'meta' => $component['meta'] ?? [],
						'project' => $project
					]))->save();
					} catch (\Exception $e) {
				}
				
			}
			
		}
	}