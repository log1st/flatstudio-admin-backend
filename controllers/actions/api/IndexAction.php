<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 10:33
 */

namespace app\controllers\actions\api;


use app\controllers\MetaController;
use app\models\Project;
use yii\base\Action;

/**
 * Class IndexAction
 * @package app\controllers\actions\api
 *
 * @property MetaController $controller
 */
class IndexAction extends Action
{
	public function run()
	{

		return $this->controller->jsonResponse(200, null, [
		]);
	}
}