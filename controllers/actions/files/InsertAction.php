<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 12.06.2018
 * Time: 2:49
 */

namespace app\controllers\actions\files;


use app\components\UploadedFile;
use app\controllers\FilesController;
use app\models\File;
use yii\base\Action;

/**
 * Class InsertAction
 * @package app\controllers\actions\files
 *
 * @property FilesController $controller
 */
class InsertAction extends Action
{
	public function run() {
		$r = \Yii::$app->request;

		$model = new File([
			'scenario' => 'insert'
		]);

		$data = $r->post();

		$data['file'] = UploadedFile::getInstanceByName('file');
		
		if (!$model->load($data, '')) {
			return $this->controller->jsonResponse(400, 'Wrong request');
		}

		if (!$model->validate()) {
			return $this->controller->jsonResponse(400, 'Error while validation', [
				'errors' => $model->getErrors()
			]);
		}
		
		$file = '/upload/' . uniqid() . '.' . $model->file->extension;

		$model->file->saveAs(dirname(__DIR__, 3) . '/web' . $file);

		return $this->controller->jsonResponse(200, 'File has been uploaded', [
			'file' => $file
		]);
	}
}