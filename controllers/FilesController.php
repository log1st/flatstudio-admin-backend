<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 12.06.2018
 * Time: 2:48
 */

namespace app\controllers;


use app\components\BaseJsonController;
use app\controllers\actions\files\InsertAction;

class FilesController extends BaseJsonController
{
	public function actions() {
		$this->enableCsrfValidation = false;
		return [
			'insert' => [
				'class' => InsertAction::class
			]
		];
	}
}