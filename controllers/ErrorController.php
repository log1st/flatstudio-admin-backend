<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:45
 */

namespace app\controllers;


use app\components\BaseJsonController;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class ErrorController extends BaseJsonController
{
	public function actionIndex()
	{
		$handler = \Yii::$app->errorHandler;

		/** @var HttpException $exception */
		$exception = $handler->exception;

		return $this->jsonResponse($exception->statusCode, $exception->getMessage(), ArrayHelper::merge([], YII_DEBUG ? [
			'trace' => array_map(function ($item) {
				return [
					'file' => $item['file'] . ':' . $item['line']
				];
			}, $exception->getTrace())
		] : []));
	}
}