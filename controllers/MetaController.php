<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 10:32
 */

namespace app\controllers;


use app\components\BaseJsonController;
use app\controllers\actions\api\IndexAction;

class MetaController extends BaseJsonController
{
	public function actions() {
		return [
			'index' => [
				'class' => IndexAction::class
			]
		];
	}
}