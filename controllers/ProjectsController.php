<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:36
 */

namespace app\controllers;


use app\components\BaseJsonController;
use app\controllers\actions\projects\IndexAction;
use app\controllers\actions\projects\InsertAction;
use app\controllers\actions\projects\UpdateAction;
use app\controllers\actions\projects\ViewAction;
use yii\filters\VerbFilter;

class ProjectsController extends BaseJsonController
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'index' => ['GET'],
					'view' => ['GET'],
					'insert' => ['POST'],
					'update' => ['POST']
				]
			]
		];
	}

	public function actions() {
		$this->enableCsrfValidation = false;
		return [
			'index' => [
				'class' => IndexAction::class
			],
			'view' => [
				'class' => ViewAction::class
			],
			'insert' => [
				'class' => InsertAction::class
			],
			'update' => [
				'class' => UpdateAction::class
			]
		];
	}
}