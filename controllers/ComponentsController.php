<?php
/**
 * Created by PhpStorm.
 * User: log1s
 * Date: 04.06.2018
 * Time: 5:36
 */

namespace app\controllers;


use app\components\BaseJsonController;
use app\controllers\actions\components\SaveAction;
use app\controllers\actions\components\ValidateAction;
use yii\filters\VerbFilter;

class ComponentsController extends BaseJsonController
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'validate' => ['POST'],
					'save' => ['POST'],
				]
			]
		];
	}

	public function actions() {
		$this->enableCsrfValidation = false;
		return [
			'save' => [
				'class' => SaveAction::class
			],
			'validate' => [
				'class' => ValidateAction::class
			],
		];
	}
}